class CreateSalespeople < ActiveRecord::Migration
  def change
    create_table :salespeople do |t|
      t.integer :sale_manager_id
      t.string :fname
      t.string :lname
      t.string :phone

      t.timestamps null: false
    end
  end
end
