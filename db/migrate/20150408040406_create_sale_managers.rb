class CreateSaleManagers < ActiveRecord::Migration
  def change
    create_table :sale_managers do |t|
      t.string :fname
      t.string :lname
      t.string :phone

      t.timestamps null: false
    end
  end
end
