module ApplicationHelper
  def grab_boolean(boolean)
    boolean ? 'Yes' : 'No'
  end
end
