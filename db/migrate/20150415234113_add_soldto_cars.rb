class AddSoldtoCars < ActiveRecord::Migration
  def change
    add_column :cars, :sold, :boolean, :default => true
  end
end
