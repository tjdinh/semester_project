class Car < ActiveRecord::Base
  has_many :quotes
  before_save :set_markup

  def full_model
    "#{color.capitalize} #{model.capitalize} #{make.capitalize} "
  end

  #Search for the Model, Color, or Vin number
  def self.search(search)
    if search
     where('model LIKE ? OR color LIKE ? OR vin LIKE ?', "%#{search}%", "%#{search}%","%#{search}%")
    else
      self.all
    end
  end

  #Validation
  validates :make, presence: true,
            format: {with:  /\A[a-zA-Z]+\z/,
                     message: "only allows letters"}
  validates :model, presence: true,
            format: {with:  /\A[a-zA-Z]+\z/,
                     message: "only allows letters"}
  validates :vin, presence: true,
            format: {with: /\A[a-zA-Z0-9]+\z/,
                    message: "no special character are allow"},
            length: {is: 17},
            uniqueness: true
  validates :color, presence: true,
            format: {with:  /\A[a-zA-Z]+\z/,
                     message: "only allows letters"}
  validates :year, presence: true,
            numericality: { only_integer: true }
  validates :msrp, presence: true,
            numericality: true

  private
  def set_markup
    self.markup = (msrp * 0.10)+msrp
  end
end
