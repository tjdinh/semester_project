class DeleteSoldToCar < ActiveRecord::Migration
  def change
    remove_columns :cars, :sold, :boolean
  end
end
