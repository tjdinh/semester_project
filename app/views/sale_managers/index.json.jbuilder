json.array!(@sale_managers) do |sale_manager|
  json.extract! sale_manager, :id, :fname, :lname, :phone
  json.url sale_manager_url(sale_manager, format: :json)
end
