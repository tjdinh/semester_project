class AddColumnTotalAmountToFinance < ActiveRecord::Migration
  def change
    add_column :finances, :finalize_price, :decimal
  end
end
