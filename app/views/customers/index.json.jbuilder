json.array!(@customers) do |customer|
  json.extract! customer, :id, :fname, :lname, :phone, :address, :state, :city
  json.url customer_url(customer, format: :json)
end
