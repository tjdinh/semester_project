# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/


updateMarkup = ->
  selection_id = $('#quote_car_id').val()
  $.getJSON '/cars/' + selection_id + '/markup', {},(json, response) ->
    getMarkup = json['markup']
    $('#markup').text getMarkup
    $('#markup').toCurrency({
      unit: '$'
    })
    getSaleTax = getMarkup * 0.043
    priceString = getSaleTax.toFixed(2)
    $('#quote_sale_tax').val(priceString)
    getTotal = parseFloat(getMarkup) + parseFloat(getSaleTax)
    Total = getTotal.toFixed(2)
    $('#quote_total').val(Total)

$(document).on 'page:change', ->
  if $('#quote_car_id').length > 0
    updateMarkup()
  $('#quote_car_id').change -> updateMarkup()
#  $('#quote_sale_tax').val -> getSaleTax()