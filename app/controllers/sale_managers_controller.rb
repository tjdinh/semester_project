class SaleManagersController < ApplicationController
  before_action :set_sale_manager, only: [:show, :edit, :update, :destroy]

  # GET /sale_managers
  # GET /sale_managers.json
  def index
    @sale_managers = SaleManager.all
  end

  # GET /sale_managers/1
  # GET /sale_managers/1.json
  def show
    @getthis = SaleManager.find(1)
  end

  # GET /sale_managers/new
  def new
    @sale_manager = SaleManager.new
  end

  # GET /sale_managers/1/edit
  def edit
  end

  # POST /sale_managers
  # POST /sale_managers.json
  def create
    @sale_manager = SaleManager.new(sale_manager_params)

    respond_to do |format|
      if @sale_manager.save
        format.html { redirect_to @sale_manager, notice: 'Sale manager was successfully created.' }
        format.json { render :show, status: :created, location: @sale_manager }
      else
        format.html { render :new }
        format.json { render json: @sale_manager.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /sale_managers/1
  # PATCH/PUT /sale_managers/1.json
  def update
    respond_to do |format|
      if @sale_manager.update(sale_manager_params)
        format.html { redirect_to @sale_manager, notice: 'Sale manager was successfully updated.' }
        format.json { render :show, status: :ok, location: @sale_manager }
      else
        format.html { render :edit }
        format.json { render json: @sale_manager.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /sale_managers/1
  # DELETE /sale_managers/1.json
  def destroy
     if @sale_manager.salespersons.count < 0
    @sale_manager.destroy
    respond_to do |format|
      format.html { redirect_to sale_managers_url, notice: 'Sale manager was successfully destroyed.' }
      format.json { head :no_content }
    end
  else
    redirect_to sale_managers_path, notice: "Cannot delete sales manager because they are still managing sales people"
       end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_sale_manager
      @sale_manager = SaleManager.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def sale_manager_params
      params.require(:sale_manager).permit(:fname, :lname, :phone)
    end
end
