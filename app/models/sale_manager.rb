class SaleManager < ActiveRecord::Base
  has_many :salespersons

  def full_SM_name
    "#{fname} #{lname}"
  end

  #validation
  validates :fname, presence: true,
            format: { with: /\A[a-zA-Z]+\z/,
                      message: "only allows letters" }
  validates :lname, presence: true,
            format: { with: /\A[a-zA-Z]+\z/,
                     message: "only allows letters" }
  validates :phone,
            presence: true,
            length: {minimum: 10, maximum: 11}

end
