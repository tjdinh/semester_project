class QuotesController < ApplicationController
  before_action :set_quote, only: [:show, :edit, :update, :destroy]

  # GET /quotes
  # GET /quotes.json
  def index
    @quotes = Quote.all
  end
  # GET /quotes/1
  # GET /quotes/1.json
  def show
  end

  #Get the Quotes JSON
  def quote
      logger.info "Markup Price Called"
      getQuote = Quote.find(params[:id])
      render :json => getQuote.to_json(
                                :include =>{
                                    :customer => {
                                        :method => [:full_Cust_name]
                                    }
                                }
                            )
  end


  # GET /quotes/new
  def new
    @quote = Quote.new
  end

  # GET /quotes/1/edit
  def edit
  end

  # POST /quotes
  # POST /quotes.json
  def create
    @quote = Quote.new(quote_params)

    respond_to do |format|
      if @quote.save
        format.html { redirect_to @quote, notice: 'Quote was successfully created.' }
        format.json { render :show, status: :created, location: @quote }
      else
        format.html { render :new }
        format.json { render json: @quote.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /quotes/1
  # PATCH/PUT /quotes/1.json
  def update
    if (@quote.toa == false)
    respond_to do |format|
      if @quote.update(quote_params)
        format.html { redirect_to @quote, notice: 'Quote was successfully updated.' }
        format.json { render :show, status: :ok, location: @quote }
      else
        format.html { render :edit }
        format.json { render json: @quote.errors, status: :unprocessable_entity }
      end
    end
    else
      redirect_to quotes_path, notice: 'Cannot edit quote that is already been sold'
    end
  end

  # DELETE /quotes/1
  # DELETE /quotes/1.json
  def destroy
    if (@quote.toa == false)
    @quote.destroy
    respond_to do |format|
      format.html { redirect_to quotes_url, notice: 'Quote was successfully destroyed.' }
      format.json { head :no_content }
    end
    else
      redirect_to quotes_path, notice: 'Cannot delete quote that is already been sold'
      end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_quote
      @quote = Quote.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def quote_params
      params.require(:quote).permit(:car_id, :salesperson_id, :customer_id, :sale_tax, :total, :toa)
    end
end
