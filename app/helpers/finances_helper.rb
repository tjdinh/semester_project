module FinancesHelper

  def payment_type
    [
        ['Cash'],
        ['Payment Plan']
    ]
  end

  def num_period
    [
        ['3'],
        ['4'],
        ['5']
    ]
  end

  def customer_name(name)
    Quote.find(name).customer.fname
  end

end
