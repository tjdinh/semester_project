class Customer < ActiveRecord::Base
  has_many :quotes
  belongs_to :customer_type

  def full_Cust_name
    "#{fname.capitalize} #{lname.capitalize}"
  end

  #Searc
  def self.search(search)
    if search
      where('fname LIKE ? OR lname LIKE ?', "%#{search}%", "%#{search}%")
    else
       self.all
    end
  end


  #validation
  validates :fname, presence: true,
            format: { with: /\A[a-zA-Z]+\z/,
                      message: "only allows letters" }
  validates :lname, presence: true,
            format: { with: /\A[a-zA-Z]+\z/,
                      message: "only allows letters" }
  validates :address,
            presence: true
  validates :phone,
            presence: true,
            length: {minimum: 10, maximum: 11}
  validates :state,
            presence: true
  validates :city,
            presence: true

  end
