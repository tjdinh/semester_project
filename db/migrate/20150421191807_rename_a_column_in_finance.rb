class RenameAColumnInFinance < ActiveRecord::Migration
  def change
    remove_columns :finances, :length, :string
    add_column :finances, :principal, :string
  end
end
