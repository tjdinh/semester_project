class CreateQuotes < ActiveRecord::Migration
  def change
    create_table :quotes do |t|
      t.integer :car_id
      t.integer :salesperson_id
      t.integer :customer_id
      t.decimal :sale_tax
      t.decimal :total
      t.boolean :toa

      t.timestamps null: false
    end
  end
end
