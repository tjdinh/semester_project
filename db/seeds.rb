# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
SaleManager.delete_all
salemanager =
    %w{1 Chris Vu 8323809900},
    %w{2 Arron Kuhns 8328908010}

salemanager.each do | i,f,l,p |
  SaleManager.create(id: i, fname:f , lname:l, phone:p)
end

Salesperson.delete_all
salepeople =
    %w{Zap To 7139309310 1},
    %w{Thomas Le 7137807890 1},
    %w{Nathanael Stehle 8323801380 1},
    %w{Cherryl Lager 8323801380 1},
    %w{Wilhelmina Olivares 3183803180 1},
    %w{Mimi Heffner 3813891380 2},
    %w{Carmela Abernathy 8323801380 2},
    %w{Norris Ribble 8327776666 2},
    %w{Leanna Forshey 8325553333 2},
    %w{Tamesha Canedy 7132223114 2}

salepeople.each do |f,l,p,sm|
  Salesperson.create(fname:f, lname:l,phone:p,sale_manager_id:sm)
end

Car.delete_all
car =
    %w(18500 Black Honda Fit 2015 true 99998888777766661),
    %w(19000 Red Honda Civic 2015 true 11112222333344442),
    %w(20000 Blue Honda Accord 2015 true 55556666777788883),
    %w(15000 White Honda Fit 2015 true 12121313141415151),
    %w(17500 Yellow Honda Fit 2014 true 21212323242425252),
    %w(19700 Black Honda Accord	2015 true 90909898979796963),
    %w(16550 Red Honda Accord	2015 true 45454646474748481),
    %w(17400 Black Honda Civic 2015 true 14511323321421412),
    %w(23000 Red Honda CRZ 2015 true 65215212521456113),
    %w(23500 Black Honda CRZ 2015 true 32146892421042111)
car.each do |mp,c,mo,ma,y,s,v|
    Car.create!(color:c,model:mo,make:ma,msrp:mp,year:y,vin:v,sold:s)
end

CustomerType.delete_all
    CustomerType.create(id: 1, name: 'Prospective')
    CustomerType.create(id: 2, name: 'Former')

Report.delete_all
    Report.create(id: 1, name: 'Johnny Dinh')



