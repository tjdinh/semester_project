class Search < ActiveRecord::Base
  def customers
    @customers ||= find_customers
  end

  private
  def find_customers
    customers = Customer.order(:fname)
    customers = customers.where('fname LIKE ? OR lname LIKE ?', "%#{keywords}%", "%#{keywords}%") if keywords.present?
    customers = customers.where(customer_type_id: customer_type_id) if customer_type_id.present?
    customers
  end
end