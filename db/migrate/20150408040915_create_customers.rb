class CreateCustomers < ActiveRecord::Migration
  def change
    create_table :customers do |t|
      t.string :fname
      t.string :lname
      t.string :phone
      t.string :address
      t.string :state
      t.string :city

      t.timestamps null: false
    end
  end
end
