class Finance < ActiveRecord::Base
  belongs_to :quote
  belongs_to :report
  before_save :add_report, :checkDropDown, :payment_plan_finalize

  def checkDropDown
    if self.payment_type == 'Cash'
      self.interest = 'N/A'
      self.period = 'N/A'
      self.finalize_price = self.principal
    end
   end


  def self.calculate_amortization_table(principal = 0,interest = 0,period = 0)
    Rails.logger.debug "Inputs: #{principal} #{interest} #{period}"
    calculation = []
    number_of_month = (period * 12.0)-1.0
    interest_per_period = (interest/100.0)/ 12.0
    # number_per_payment = (principal*interest_per_period)/(1.0-(1.0+interest_per_period)**(-1.0*(period*12.0)))
     number_per_payment = (BigDecimal(principal)*((BigDecimal(interest)/100.0)/ 12.0))/(1.0-(1.0+((BigDecimal(interest)/100.0)/ 12.0))**(-1.0*(BigDecimal(period)*12.0)))
    count = 0
    principal = BigDecimal(principal)
    (0..number_of_month).each do
      calculation << count+= 1
      calculation << '$%.2f' % number_per_payment
      interest_per_month = principal *  interest_per_period
      calculation << '$%.2f' % interest_per_month
      principal_per_month = BigDecimal(number_per_payment) - BigDecimal(interest_per_month)
      calculation << '$%.2f' % principal_per_month
      ending_balance = principal -= principal_per_month
      if ending_balance < 0
        ending_balance = 0
      end
      calculation << '$%.2f' % ending_balance.round(2)
    end
    return calculation
  end

  def calculate_amortization_table
    return self.class.calculate_amortization_table(self.principal,self.interest,self.period)
  end

private
  def add_report
    self.report_id = 1
  end

  def payment_plan_finalize
    if self.payment_type == 'Payment Plan'
      self.finalize_price = ((BigDecimal(self.principal)*((BigDecimal(self.interest)/100.0)/ 12.0))/(1.0-(1.0+((BigDecimal(self.interest)/100.0)/ 12.0))**(-1.0*(BigDecimal(self.period)*12.0))))*(BigDecimal(self.period)*12)
    end
  end
end
