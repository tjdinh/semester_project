json.array!(@finances) do |finance|
  json.extract! finance, :id, :quote_id, :report_id, :payment_type, :interest, :period, :principal, :finalize_price
  json.url finance_url(finance, format: :json)
end
