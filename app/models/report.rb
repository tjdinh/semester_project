class Report < ActiveRecord::Base
  has_many :finances


  def calculation_total_gross_revenue
    total_gross_revenue = Finance.sum("finalize_price") - Car.where(:sold => false).sum("msrp")
  end
  def calculation_net_profit
    net_profit = self.calculation_total_gross_revenue - self.calculation_total_sale_tax
  end
  def calculation_total_sale_tax
    sale_tax = Quote.where(:toa => true).sum("sale_tax")
  end
end
