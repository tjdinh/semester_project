class ChangeMsrpToCars < ActiveRecord::Migration
  def change
    remove_column :cars, :MSRP, :decimal
    add_column :cars, :msrp, :decimal
  end
end
