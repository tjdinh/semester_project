require 'test_helper'

class SaleManagersControllerTest < ActionController::TestCase
  setup do
    @sale_manager = sale_managers(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:sale_managers)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create sale_manager" do
    assert_difference('SaleManager.count') do
      post :create, sale_manager: { fname: @sale_manager.fname, lname: @sale_manager.lname, phone: @sale_manager.phone }
    end

    assert_redirected_to sale_manager_path(assigns(:sale_manager))
  end

  test "should show sale_manager" do
    get :show, id: @sale_manager
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @sale_manager
    assert_response :success
  end

  test "should update sale_manager" do
    patch :update, id: @sale_manager, sale_manager: { fname: @sale_manager.fname, lname: @sale_manager.lname, phone: @sale_manager.phone }
    assert_redirected_to sale_manager_path(assigns(:sale_manager))
  end

  test "should destroy sale_manager" do
    assert_difference('SaleManager.count', -1) do
      delete :destroy, id: @sale_manager
    end

    assert_redirected_to sale_managers_path
  end
end
