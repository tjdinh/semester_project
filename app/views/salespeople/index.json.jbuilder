json.array!(@salespeople) do |salesperson|
  json.extract! salesperson, :id, :sale_manager_id, :fname, :lname, :phone
  json.url salesperson_url(salesperson, format: :json)
end
