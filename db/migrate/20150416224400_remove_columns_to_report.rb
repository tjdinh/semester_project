class RemoveColumnsToReport < ActiveRecord::Migration
  def change
    remove_columns :reports ,:total_gross_revenue, :decimal
    remove_columns :reports, :net_profit, :decimal
    remove_columns :reports, :sale_tax_total, :decimal
    add_column :reports, :name, :string
  end
end
