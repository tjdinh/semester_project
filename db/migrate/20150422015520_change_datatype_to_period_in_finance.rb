class ChangeDatatypeToPeriodInFinance < ActiveRecord::Migration
  def change
    remove_column :finances, :period, :string
    add_column :finances, :period, :integer
  end
end
