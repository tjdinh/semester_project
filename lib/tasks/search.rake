desc "Remove search after 20 searches"
task :remove_old_searches => :environment do
  Search.delete_all ["search_id <= ?", 20]
 end
