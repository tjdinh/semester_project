# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

updateNotes = ->
  selection_id = $('#finance_quote_id').val()
  $.getJSON '/quotes/' + selection_id + '/quote', {},(json, response) ->
    getPrincipal = json['total']
    formatPrincipal = parseFloat(getPrincipal).toFixed(2)
    $('#finance_principal').val(formatPrincipal)
    $('#quote').text json['id']
    $('#quote').append '</br>Name :' +capitalizeFirstLetter(json.customer.fname)+' '+capitalizeFirstLetter(json.customer.lname)
    $('#quote').append '</br>Car Total $:' +formatPrincipal

capitalizeFirstLetter = (string) ->
  string.charAt(0).toUpperCase() + string.slice(1)

toggleFields = ->
  if $('#finance_payment_type').val() == 'Payment Plan'
    $('#Payment_plan').show()
  else
    $('#Payment_plan').hide()
  return

build_table = ->
  if $('#finance_payment_type').val() == 'Payment Plan'
    time =  $("#finance_period").val()
    principal = $("#finance_principal").val()
    interest_rate = $("#finance_interest").val()
    $.get "/finances/amortization",{principal: principal,time: time, rate:interest_rate}


$(document).on 'page:change', ->
  if $('#finance_quote_id').length > 0
    updateNotes()
  $('#finance_quote_id').change -> updateNotes()
  $(document).ready -> toggleFields()
  $('#finance_payment_type').change ->
    toggleFields()
  $('#finance_principal,#finance_interest,#finance_period').bind 'change keyup mouseup mousewheel', ->
    build_table()
  build_table()


