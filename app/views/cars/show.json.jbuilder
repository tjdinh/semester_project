json.extract! @car, :id, :make, :model, :vin, :color, :year, :msrp, :markup, :created_at, :updated_at
