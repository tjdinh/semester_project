json.array!(@reports) do |report|
  json.extract! report, :id, :total_gross_revenue, :net_profit, :sale_tax_total
  json.url report_url(report, format: :json)
end
