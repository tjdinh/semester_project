class Quote < ActiveRecord::Base
  belongs_to :salesperson
  belongs_to :car
  belongs_to :customer
  has_one :finance
  before_save :calculate_sale_tax, :calculate_total
  after_save :set_sold

  def set_sold
    if self.toa == true
      Car.update(self.car_id,:sold => false)
    end
  end

  def full_quote
    "#{id} | #{car.full_model}"
  end

private
  def calculate_sale_tax
    self.sale_tax = self.car.markup * 0.043
  end
  def calculate_total
    self.total = self.car.markup + (self.car.markup * 0.043)
  end
end
