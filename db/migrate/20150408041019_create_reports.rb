class CreateReports < ActiveRecord::Migration
  def change
    create_table :reports do |t|
      t.decimal :total_gross_revenue
      t.decimal :net_profit
      t.decimal :sale_tax_total

      t.timestamps null: false
    end
  end
end
